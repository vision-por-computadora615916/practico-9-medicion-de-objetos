# Practico 9 - Medición de objetos

[Medición_de_objetos.py](https://gitlab.com/vision-por-computadora615916/practico-9-medicion-de-objetos/-/blob/main/Medici%C3%B3n_de_objetos.py?ref_type=heads) este script en Python utiliza OpenCV para permitir al usuario seleccionar puntos en una imagen, aplicar una transformación de perspectiva y medir distancias en la imagen transformada en unidades reales.

## Funcionalidades

- Configuración Inicial:

Importa las bibliotecas necesarias.

Define las dimensiones reales en centímetros.

Define las coordenadas de los puntos de la imagen de entrada.

Carga la imagen `perspectiva.jpg` y la redimensiona a 800x600 píxeles.

Define los puntos de destino para la transformación de perspectiva con un aspecto mantenido el de la imagen.

- Funciones:

measure_distance: Callback que se activa al hacer clic en la imagen. Permite seleccionar dos puntos y calcula la distancia entre ellos en centímetros basándose en la altura real y la altura de la imagen transformada.

- Variables Globales:

points: Lista para almacenar los puntos seleccionados por el usuario en la imagen.

warped_image: Imagen transformada por perspectiva y mostrada al usuario.

- Bucle Principal:

        Muestra la imagen transformada y escucha las teclas del usuario.

        q: Salir del programa.

        r: Reiniciar la imagen original para empezar de nuevo.

        h: Aplicar transformación de perspectiva basada en los puntos y las dimensiones reales.
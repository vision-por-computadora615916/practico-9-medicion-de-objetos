#! /usr/bin/env python
#-*-coding: utf-8-*-


#ancho de revista 21 y 27 de alto
# figurita ancho 8 y 10 de alto
# individual 45 y 30

import cv2
import numpy as np

# Dimensiones reales
real_alto= 45  # centimetros
real_ancho = 30  # centimetros

r_points = [(115, 93), (437, 88), (729, 283), (261, 347)] 

img_perspectiva = cv2.imread('perspectiva.jpg')


width = 800
height = 600

# Redimensionar la imagen
warped_image = cv2.resize(img_perspectiva, (width, height))

pts_image = np.array(r_points, dtype="float32")
aspect_ratio = real_alto/ real_ancho
new_height = 500
new_width = int(new_height / aspect_ratio)
pts_dst = np.array([[0, 0], [new_width - 1, 0], [new_width - 1, new_height - 1], [0, new_height - 1]], dtype="float32")



def measure_distance(event, x, y, flags, param):
	global points, warped_image
	if event == cv2.EVENT_LBUTTONDOWN:
		points.append((x, y))
		if len(points) == 2:
			cv2.line(warped_image, points[0], points[1], (0, 0, 0), 2)
			pixel_distance = np.sqrt((points[0][0] - points[1][0]) ** 2 + (points[0][1] - points[1][1]) ** 2)
			meter_distance = pixel_distance * (real_alto/ new_height)
			print(f'Distancia: {meter_distance:.2f} centimetros')
			points = []
			cv2.putText(warped_image, f'{meter_distance:.2f} cm', (x, y), cv2.FONT_HERSHEY_SIMPLEX, 0.4, (0,0,0), 1, cv2.LINE_AA)
			cv2.imshow('Warped Image', warped_image)

points = []



while(1) :	
	cv2.imshow('Warped Image', warped_image)
	cv2.setMouseCallback('Warped Image', measure_distance)
	k = cv2.waitKey(1) & 0xFF

	if k == 113 : #Salir, letra q
		break
		
	if k == 114: # Recuperar foto inicial, letra r
		cv2.destroyAllWindows()
		i=0
		img = cv2.imread('perspectiva.jpg')
		
		points = []

		# Redimensionar la imagen
		warped_image = cv2.resize(img_perspectiva, (width, height))
		#cv2.setMouseCallback('image',draw_circle)
	
	if k == 104: # Letra h para seleccionar 4 puntos no colineales
		cv2.destroyAllWindows()
		
		matrix = cv2.getPerspectiveTransform(pts_image, pts_dst)

		
		warped_image = cv2.warpPerspective(warped_image, matrix, (new_width, new_height))
			
		
cv2.destroyAllWindows()


